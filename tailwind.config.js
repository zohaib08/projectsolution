/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  purge: ['./index.html', './src/**/*.{vue,js}'],
  darkMode: false,
  content: [],
  theme: {
    extend: {},
  },
  plugins: [],
}