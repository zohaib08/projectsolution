import Vue from 'vue'
import VueRouter from 'vue-router'
import MoviesView from '../views/Movies.vue'
import MovieDetail from '../views/MovieDetail.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/movies',
  },
  {
    path: '/movies',
    name: 'Movies_',
    component: MoviesView
  },
  { path: '/movies/:imdbID', name: 'movies', component: MovieDetail, props: true },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
