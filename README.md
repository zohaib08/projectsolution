# cinemaquest

## Project setup

```
npm install

```

## Api key

```
Please Get your api key from http://www.omdbapi.com/ and replace it with api key used in this application
```

### Compiles and hot-reloads for development

```
npm run serve it will run project on 8080 port
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```
